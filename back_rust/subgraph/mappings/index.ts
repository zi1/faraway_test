/* eslint-disable prefer-const */
import { Factory, Collection, NFT } from "../generated/schema";
import { CollectionCreated, TokenMinted } from "../generated/NFTFactory/NFTFactory";

// Constants
let CONTRACT_ADDRESS = "0x61Ad3784a57080c300CBc81432454D9a355A9BaB";

export function handleCollectionCreated(event: CollectionCreated): void {
  let factory = Factory.load(CONTRACT_ADDRESS);
  if (factory === null) {
    // Factory
    factory = new Factory(CONTRACT_ADDRESS);
  }
  factory.save();

  // Collection
  const collection = new Collection(event.params.collection.toHex());
  collection.factory = factory.id;
  collection.name = event.params.name;
  collection.symbol = event.params.symbol;
  collection.save();
}

export function handleTokenMinted(event: TokenMinted): void {
  // NFT
  const nft = new NFT(event.params.tokenId.toHex());
  nft.collection = event.params.collection.toHex();
  nft.minter = event.params.recipient;
  nft.uri = event.params.tokenUri;
  nft.createdAt = event.block.timestamp;
  nft.save();
}
