// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

interface INFTCollection {
    /**
     * @notice Initialize nft collection.
     * @param name_: collection name
     * @param symbol_: collection symbol
     *
     * @dev Callable by factory
     *
     */
    function initialize(
        string memory name_,
        string memory symbol_
    ) external;

    /**
     * @notice Mint nft.
     * @param _to: recipient address
     *
     * @dev Callable by factory
     *
     */
    function safeMint(
        address _to
    ) external returns (
        uint256,
        string memory
    );

    /**
     * @notice Setting base uri for nft collection.
     * @param _uri: new base uri
     *
     * @dev Callable by factory
     *
     */
    function setUri(
        string memory _uri
    ) external;

    /**
     * @notice Return token URI
     * @param tokenId: token id
     *
     */
    function tokenURI(
        uint256 tokenId
    ) external view virtual returns (string memory);
}
