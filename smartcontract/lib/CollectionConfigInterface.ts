import NetworkConfigInterface from '../lib/NetworkConfigInterface';

export default interface CollectionConfigInterface {
  testnet: NetworkConfigInterface;
  mainnet: NetworkConfigInterface;
  contractName: string;
  contractAddress: string|null;
};
