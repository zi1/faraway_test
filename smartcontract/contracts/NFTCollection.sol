// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import '../lib/INFTCollection.sol';
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract NFTCollection is INFTCollection, ERC721 {
    using Counters for Counters.Counter;

    string private _name;
    string private _symbol;
    string private baseURI = 'ipfs://';
    string private uriSuffix = '.json';
    Counters.Counter private _tokenIdCounter;
    address factory;

    constructor() ERC721('NFTFactoryCollection', 'NFTFC') {
        factory = msg.sender;
    }

    /**
     * @notice Initialize nft collection.
     * @param name_: collection name
     * @param symbol_: collection symbol
     *
     * @dev Callable by factory
     *
     */
    function initialize(
        string memory name_,
        string memory symbol_
    ) external {
        require(msg.sender == factory, 'Only factory');
        _name = name_;
        _symbol = symbol_;
    }

    /**
     * @notice Mint nft.
     * @param _to: recipient address
     *
     * @dev Callable by factory
     *
     */
    function safeMint(
        address _to
    ) external returns (
        uint256 tokenId,
        string memory tokenUri
    ) {
        require(msg.sender == factory, 'Only factory');

        tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(_to, tokenId);
        tokenUri = tokenURI(tokenId);
    }

    /**
     * @notice Setting base uri for nft collection.
     * @param _uri: new base uri
     *
     * @dev Callable by factory
     *
     */
    function setUri(
        string memory _uri
    ) external {
        require(msg.sender == factory, 'Only factory');
        baseURI = _uri;
    }

    /**
     * @notice Return base URI
     *
     */
    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }

    /**
     * @dev See {IERC721Metadata-name}.
     */
    function name() public view virtual override returns (string memory) {
        return _name;
    }

    /**
     * @dev See {IERC721Metadata-symbol}.
     */
    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    /**
     * @notice Return token URI
     * @param tokenId: token id
     *
     */
    function tokenURI(
        uint256 tokenId
    ) public view virtual override(ERC721, INFTCollection) returns (string memory) {
        return bytes(baseURI).length > 0 ? string(abi.encodePacked(baseURI, Strings.toString(tokenId), uriSuffix)) : '';
    }
}
