import { assert } from "chai";
import { parseEther } from "ethers/lib/utils";
import { artifacts, contract } from "hardhat";
import {Contract} from "ethers";
// @ts-ignore
import {expectEvent} from "@openzeppelin/test-helpers";
import CollectionConfig from "../config/CollectionConfig";

const NFTFactory = artifacts.require("./NFTFactory.sol");
const NFTCollection = artifacts.require("./NFTCollection.sol");

contract(CollectionConfig.contractName, ([deployer, creator, mintor]) => {
  let nftFactory: Contract;
  let result: any;
  const NFT_NAME: string = "NFT_NAME";
  const NFT_SYMBOL: string = "NFT_SYMBOL";

  before(async () => {
    // Deploy Factory
    nftFactory = await NFTFactory.new({ from: deployer });
  });

  describe("TEST", async () => {
    it("Create NFT Collection", async () => {
      result = await nftFactory.createCollection(NFT_NAME, NFT_SYMBOL, {
        from: creator,
      });

      expectEvent(result, "CollectionCreated", {
        name: NFT_NAME,
        symbol: NFT_SYMBOL,
      });
    });

    it("Mint NFT", async () => {
      const collection = await nftFactory.collections(NFT_SYMBOL, {
        from: creator,
      });

      result = await nftFactory.mint(collection, {
        from: mintor,
      });

      expectEvent(result, "TokenMinted", {
        recipient: mintor,
      });
    });
  });
});
