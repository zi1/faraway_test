import CollectionConfigInterface from "../lib/CollectionConfigInterface";
import * as Networks from "../lib/Networks";

const CollectionConfig: CollectionConfigInterface = {
  testnet: Networks.bscTestnet,
  mainnet: Networks.bscMainnet,
  contractName: "NFTCollection",
  contractAddress: "0x61Ad3784a57080c300CBc81432454D9a355A9BaB",
};

export default CollectionConfig;
