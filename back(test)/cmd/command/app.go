package command

import (
	"context"
	"errors"
	"gitlab.com/zi1/faraway/back/config"
	"github.com/spf13/cobra"
)

// NewRootCommand creates the root command
func NewRootCommand(ctx context.Context) *cobra.Command {
	rootCommand := &cobra.Command{
		Use:   "nft_factory",
		Short: "Run the shared NFT Factory service",

		PersistentPreRunE: config.Setup,
		RunE: func(cmd *cobra.Command, args []string) error {
			return errors.New("please specify a command: [deploy, monitor or run]")
		},
	}

	rootCommand.Flags().StringVarP(
		&config.Filename,
		"config",
		"f",
		"config/local.yaml",
		"Relative path to the config file",
	)

	rootCommand.AddCommand(NewMonitorCommand(ctx))

	return rootCommand
}
