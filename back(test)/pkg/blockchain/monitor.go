package blockchain

import (
	"context"
	"encoding/json"
	"fmt"
	contracts "gitlab.com/zi1/faraway/back/contracts/interfaces"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"golang.org/x/sync/errgroup"
	"log"
	"math/big"
	"time"
)

// Monitor interface
type Monitor interface {
	Start(ctx context.Context, client *ethclient.Client) error
}

type monitor struct {
	contractAddress string
}

// CollectionCreatedEvent struct
type CollectionCreatedEvent struct {
	Event string `json:"event_type"`
	Collection string `json:"collection"`
	Name string `json:"name"`
  Symbol string `json:"symbol"`
	Timestamp time.Time `json:"timestamp"`
}

// TokenMintedEvent struct
type TokenMintedEvent struct {
	Event string `json:"event_type"`
	Sender string `json:"sender"`
	Collection string `json:"collection"`
  Recipient *big.Int `json:"recipient"`
  TokenId *big.Int `json:"tokenId"`
  TokenUri *big.Int `json:"tokenUri"`
	Timestamp time.Time `json:"timestamp"`
}

// NewMonitor returns a new runner instance
func NewMonitor(contractAddress string) Monitor {
	return &monitor{
		contractAddress: contractAddress,
	}
}

// Start register to listen blockchain events
func (m *monitor) Start(ctx context.Context, client *ethclient.Client) error {
	log.Printf("start monitoring at %s\n", m.contractAddress)

	err := validateContractAddress(ctx, client, m.contractAddress)
	if err != nil {
		return err
	}
	contract, err := contracts.NewContract(common.HexToAddress(m.contractAddress), client)
	if err != nil {
		return err
	}

	eg := new(errgroup.Group)
	eg.Go(func() error {
		return m.watchCollectionCreated(ctx, contract)
	})
	if err = eg.Wait(); err != nil {
		return err
	}
	return nil
}

func (m *monitor) watchCollectionCreated(ctx context.Context, contract *contracts.Contract) error {
	events := make(chan *contracts.ContractCollectionCreated)
	opts := &bind.WatchOpts{
		Start:   nil,
		Context: ctx,
	}
	subscription, err := contract.WatchCollectionCreated(opts, events, nil)
	if err != nil {
		return err
	}
	defer subscription.Unsubscribe()

	for {
		select {
		case <-ctx.Done():
			return nil
		case errChan := <-subscription.Err():
			return errChan
		case event := <-events:
			j, _ := json.MarshalIndent(
				CollectionCreatedEvent{
					Event:       "CollectionCreated",
					Collection:      event.Collection.Hex(),
					Name:  event.Name,
					Symbol:   event.Symbol,
					Timestamp: time.Now(),
				},
				"",
				"  ",
			)
			fmt.Println(string(j))
		}
	}
}

