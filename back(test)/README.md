## Contract
The contract files are at `./contracts` folder. Third-party contracts like Open Zeppelin are stored at `./contracts/vendor` folder,
while compiled contracts are stored at `./contracts/bin` folder

### Compile contracts
In order to compile the contracts the `ethereum/solc` docker image is used as following:

```shell
docker run --rm -v /Users/igorzemtsov/Projects/faraway/back/contracts:/contracts ethereum/solc:stable \
/contracts/solidity/NFTFactory.sol --bin --abi --optimize --overwrite -o /contracts/bin
```

To generate a Go interface and bindings based on the contracts, the `ethereum/client-go:alltools-latest` docker image is used as following:

```shell
docker run --rm -v /Users/igorzemtsov/Projects/faraway/back/contracts:/contracts ethereum/client-go:alltools-latest \
abigen --abi /contracts/bin/NFTFactory.abi --pkg contracts --type Contract --out \
/contracts/interfaces/nft_factory.go  --bin /contracts/bin/NFTFactory.bin
```

## Geth

Run Geth docker image as:
```shell
docker run --rm --name ethereum-node -v /Users/igorzemtsov/Projects/faraway/back/eth/:/root -p 8545:8545 -p 30303:30303 ethereum/client-go --nodiscover
```

and connect to the instance as following in order to perform CLI operations:
```shell
docker exec -it <> /bin/sh

$ geth attach /root/.ethereum/geth.ipc
```

### Monitor
In order to monitor the events generated in by the deployed contract just run the command `./nft_factory monitor`.
