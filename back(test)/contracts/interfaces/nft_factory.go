// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package contracts

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// ContractMetaData contains all meta data concerning the Contract contract.
var ContractMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"name\",\"type\":\"string\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"symbol\",\"type\":\"string\"}],\"name\":\"CollectionCreated\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"tokenUri\",\"type\":\"string\"}],\"name\":\"TokenMinted\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"name\":\"collections\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"_symbol\",\"type\":\"string\"}],\"name\":\"createCollection\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"collection\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_collection\",\"type\":\"address\"}],\"name\":\"mint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561000f575f80fd5b50611f528061001d5f395ff3fe608060405234801561000f575f80fd5b506004361061003f575f3560e01c8063176b9b751461004357806366e54d7b146100725780636a627842146100a5575b5f80fd5b610056610051366004610445565b6100ba565b6040516001600160a01b03909116815260200160405180910390f35b6100566100803660046104a5565b80516020818301810180515f825292820191909301209152546001600160a01b031681565b6100b86100b33660046104df565b610270565b005b5f806001600160a01b03165f836040516100d4919061052e565b908152604051908190036020019020546001600160a01b0316146101385760405162461bcd60e51b815260206004820152601660248201527518dbdb1b1958dd1a5bdb881a5cc81c995c5d5a5c995960521b60448201526064015b60405180910390fd5b5f604051806020016101499061037a565b6020820181038252601f19601f8201166040525090505f8484604051602001610173929190610549565b604051602081830303815290604052805190602001209050808251602084015ff560405163266c45bb60e11b81529093506001600160a01b03841690634cd88b76906101c590889088906004016105a2565b5f604051808303815f87803b1580156101dc575f80fd5b505af11580156101ee573d5f803e3d5ffd5b50505050825f85604051610202919061052e565b90815260405190819003602001812080546001600160a01b039384166001600160a01b0319909116179055908416907f3454b57f2dca4f5a54e8358d096ac9d1a0d2dab98991ddb89ff9ea17462606179061026090889088906105a2565b60405180910390a2505092915050565b6001600160a01b0381166102bf5760405162461bcd60e51b815260206004820152601660248201527518dbdb1b1958dd1a5bdb881a5cc81c995c5d5a5c995960521b604482015260640161012f565b6040516340d097c360e01b81523360048201525f9081906001600160a01b038416906340d097c3906024015f604051808303815f875af1158015610305573d5f803e3d5ffd5b505050506040513d5f823e601f3d908101601f1916820160405261032c91908101906105cf565b91509150826001600160a01b03167fc9fee7cd4889f66f10ff8117316524260a5242e88e25e0656dfb3f4196a2191733848460405161036d9392919061064d565b60405180910390a2505050565b6118a98061067483390190565b634e487b7160e01b5f52604160045260245ffd5b604051601f8201601f1916810167ffffffffffffffff811182821017156103c4576103c4610387565b604052919050565b5f67ffffffffffffffff8211156103e5576103e5610387565b50601f01601f191660200190565b5f82601f830112610402575f80fd5b8135610415610410826103cc565b61039b565b818152846020838601011115610429575f80fd5b816020850160208301375f918101602001919091529392505050565b5f8060408385031215610456575f80fd5b823567ffffffffffffffff8082111561046d575f80fd5b610479868387016103f3565b9350602085013591508082111561048e575f80fd5b5061049b858286016103f3565b9150509250929050565b5f602082840312156104b5575f80fd5b813567ffffffffffffffff8111156104cb575f80fd5b6104d7848285016103f3565b949350505050565b5f602082840312156104ef575f80fd5b81356001600160a01b0381168114610505575f80fd5b9392505050565b5f5b8381101561052657818101518382015260200161050e565b50505f910152565b5f825161053f81846020870161050c565b9190910192915050565b5f835161055a81846020880161050c565b83519083019061056e81836020880161050c565b01949350505050565b5f815180845261058e81602086016020860161050c565b601f01601f19169290920160200192915050565b604081525f6105b46040830185610577565b82810360208401526105c68185610577565b95945050505050565b5f80604083850312156105e0575f80fd5b82519150602083015167ffffffffffffffff8111156105fd575f80fd5b8301601f8101851361060d575f80fd5b805161061b610410826103cc565b81815286602083850101111561062f575f80fd5b61064082602083016020860161050c565b8093505050509250929050565b60018060a01b0384168152826020820152606060408201525f6105c6606083018461057756fe608060405234801562000010575f80fd5b506040518060400160405280601481526020017f4e4654466163746f7279436f6c6c656374696f6e000000000000000000000000815250604051806040016040528060058152602001644e4654464360d81b815250815f90816200007591906200013f565b5060016200008482826200013f565b5050600a80546001600160a01b031916331790555062000207565b634e487b7160e01b5f52604160045260245ffd5b600181811c90821680620000c857607f821691505b602082108103620000e757634e487b7160e01b5f52602260045260245ffd5b50919050565b601f8211156200013a575f81815260208120601f850160051c81016020861015620001155750805b601f850160051c820191505b81811015620001365782815560010162000121565b5050505b505050565b81516001600160401b038111156200015b576200015b6200009f565b62000173816200016c8454620000b3565b84620000ed565b602080601f831160018114620001a9575f8415620001915750858301515b5f19600386901b1c1916600185901b17855562000136565b5f85815260208120601f198616915b82811015620001d957888601518255948401946001909101908401620001b8565b5085821015620001f757878501515f19600388901b60f8161c191681555b5050505050600190811b01905550565b61169480620002155f395ff3fe608060405234801561000f575f80fd5b50600436106100fb575f3560e01c80636352211e11610093578063a22cb46511610063578063a22cb46514610225578063b88d4fde14610238578063c87b56dd1461024b578063e985e9c51461025e575f80fd5b80636352211e146101d657806370a08231146101e957806395d89b411461020a5780639b642de114610212575f80fd5b806323b872dd116100ce57806323b872dd1461017c57806340d097c31461018f57806342842e0e146101b05780634cd88b76146101c3575f80fd5b806301ffc9a7146100ff57806306fdde0314610127578063081812fc1461013c578063095ea7b314610167575b5f80fd5b61011261010d366004610f6d565b610271565b60405190151581526020015b60405180910390f35b61012f6102c2565b60405161011e9190610fdc565b61014f61014a366004610fee565b610351565b6040516001600160a01b03909116815260200161011e565b61017a610175366004611020565b6103e9565b005b61017a61018a366004611048565b6104fd565b6101a261019d366004611081565b61052e565b60405161011e92919061109a565b61017a6101be366004611048565b61058a565b61017a6101d1366004611157565b6105a4565b61014f6101e4366004610fee565b6105e7565b6101fc6101f7366004611081565b61065d565b60405190815260200161011e565b61012f6106e2565b61017a6102203660046111b7565b6106f1565b61017a6102333660046111e9565b61072b565b61017a610246366004611222565b610736565b61012f610259366004610fee565b61076e565b61011261026c366004611299565b6107ca565b5f6001600160e01b031982166380ac58cd60e01b14806102a157506001600160e01b03198216635b5e139f60e01b145b806102bc57506301ffc9a760e01b6001600160e01b03198316145b92915050565b60605f80546102d0906112ca565b80601f01602080910402602001604051908101604052809291908181526020018280546102fc906112ca565b80156103475780601f1061031e57610100808354040283529160200191610347565b820191905f5260205f20905b81548152906001019060200180831161032a57829003601f168201915b5050505050905090565b5f818152600260205260408120546001600160a01b03166103ce5760405162461bcd60e51b815260206004820152602c60248201527f4552433732313a20617070726f76656420717565727920666f72206e6f6e657860448201526b34b9ba32b73a103a37b5b2b760a11b60648201526084015b60405180910390fd5b505f908152600460205260409020546001600160a01b031690565b5f6103f3826105e7565b9050806001600160a01b0316836001600160a01b0316036104605760405162461bcd60e51b815260206004820152602160248201527f4552433732313a20617070726f76616c20746f2063757272656e74206f776e656044820152603960f91b60648201526084016103c5565b336001600160a01b038216148061047c575061047c81336107ca565b6104ee5760405162461bcd60e51b815260206004820152603860248201527f4552433732313a20617070726f76652063616c6c6572206973206e6f74206f7760448201527f6e6572206e6f7220617070726f76656420666f7220616c6c000000000000000060648201526084016103c5565b6104f883836107f7565b505050565b6105073382610864565b6105235760405162461bcd60e51b81526004016103c590611302565b6104f8838383610939565b600a545f906060906001600160a01b0316331461055d5760405162461bcd60e51b81526004016103c590611353565b6009549150610570600980546001019055565b61057a8383610ad1565b6105838261076e565b9050915091565b6104f883838360405180602001604052805f815250610736565b600a546001600160a01b031633146105ce5760405162461bcd60e51b81526004016103c590611353565b60066105da83826113c6565b5060076104f882826113c6565b5f818152600260205260408120546001600160a01b0316806102bc5760405162461bcd60e51b815260206004820152602960248201527f4552433732313a206f776e657220717565727920666f72206e6f6e657869737460448201526832b73a103a37b5b2b760b91b60648201526084016103c5565b5f6001600160a01b0382166106c75760405162461bcd60e51b815260206004820152602a60248201527f4552433732313a2062616c616e636520717565727920666f7220746865207a65604482015269726f206164647265737360b01b60648201526084016103c5565b506001600160a01b03165f9081526003602052604090205490565b6060600180546102d0906112ca565b600a546001600160a01b0316331461071b5760405162461bcd60e51b81526004016103c590611353565b600861072782826113c6565b5050565b610727338383610aea565b6107403383610864565b61075c5760405162461bcd60e51b81526004016103c590611302565b61076884848484610bb7565b50505050565b60605f6008805461077e906112ca565b9050116107995760405180602001604052805f8152506102bc565b60086107a483610bea565b6040516020016107b5929190611482565b60405160208183030381529060405292915050565b6001600160a01b039182165f90815260056020908152604080832093909416825291909152205460ff1690565b5f81815260046020526040902080546001600160a01b0319166001600160a01b038416908117909155819061082b826105e7565b6001600160a01b03167f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b92560405160405180910390a45050565b5f818152600260205260408120546001600160a01b03166108dc5760405162461bcd60e51b815260206004820152602c60248201527f4552433732313a206f70657261746f7220717565727920666f72206e6f6e657860448201526b34b9ba32b73a103a37b5b2b760a11b60648201526084016103c5565b5f6108e6836105e7565b9050806001600160a01b0316846001600160a01b031614806109215750836001600160a01b031661091684610351565b6001600160a01b0316145b80610931575061093181856107ca565b949350505050565b826001600160a01b031661094c826105e7565b6001600160a01b0316146109b05760405162461bcd60e51b815260206004820152602560248201527f4552433732313a207472616e736665722066726f6d20696e636f72726563742060448201526437bbb732b960d91b60648201526084016103c5565b6001600160a01b038216610a125760405162461bcd60e51b8152602060048201526024808201527f4552433732313a207472616e7366657220746f20746865207a65726f206164646044820152637265737360e01b60648201526084016103c5565b610a1c5f826107f7565b6001600160a01b0383165f908152600360205260408120805460019290610a44908490611529565b90915550506001600160a01b0382165f908152600360205260408120805460019290610a7190849061153c565b90915550505f8181526002602052604080822080546001600160a01b0319166001600160a01b0386811691821790925591518493918716917fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef91a4505050565b610727828260405180602001604052805f815250610ce7565b816001600160a01b0316836001600160a01b031603610b4b5760405162461bcd60e51b815260206004820152601960248201527f4552433732313a20617070726f766520746f2063616c6c65720000000000000060448201526064016103c5565b6001600160a01b038381165f81815260056020908152604080832094871680845294825291829020805460ff191686151590811790915591519182527f17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31910160405180910390a3505050565b610bc2848484610939565b610bce84848484610d19565b6107685760405162461bcd60e51b81526004016103c59061154f565b6060815f03610c105750506040805180820190915260018152600360fc1b602082015290565b815f5b8115610c395780610c23816115a1565b9150610c329050600a836115cd565b9150610c13565b5f8167ffffffffffffffff811115610c5357610c536110b2565b6040519080825280601f01601f191660200182016040528015610c7d576020820181803683370190505b5090505b841561093157610c92600183611529565b9150610c9f600a866115e0565b610caa90603061153c565b60f81b818381518110610cbf57610cbf6115f3565b60200101906001600160f81b03191690815f1a905350610ce0600a866115cd565b9450610c81565b610cf18383610e16565b610cfd5f848484610d19565b6104f85760405162461bcd60e51b81526004016103c59061154f565b5f6001600160a01b0384163b15610e0b57604051630a85bd0160e11b81526001600160a01b0385169063150b7a0290610d5c903390899088908890600401611607565b6020604051808303815f875af1925050508015610d96575060408051601f3d908101601f19168201909252610d9391810190611643565b60015b610df1573d808015610dc3576040519150601f19603f3d011682016040523d82523d5f602084013e610dc8565b606091505b5080515f03610de95760405162461bcd60e51b81526004016103c59061154f565b805181602001fd5b6001600160e01b031916630a85bd0160e11b149050610931565b506001949350505050565b6001600160a01b038216610e6c5760405162461bcd60e51b815260206004820181905260248201527f4552433732313a206d696e7420746f20746865207a65726f206164647265737360448201526064016103c5565b5f818152600260205260409020546001600160a01b031615610ed05760405162461bcd60e51b815260206004820152601c60248201527f4552433732313a20746f6b656e20616c7265616479206d696e7465640000000060448201526064016103c5565b6001600160a01b0382165f908152600360205260408120805460019290610ef890849061153c565b90915550505f8181526002602052604080822080546001600160a01b0319166001600160a01b03861690811790915590518392907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef908290a45050565b6001600160e01b031981168114610f6a575f80fd5b50565b5f60208284031215610f7d575f80fd5b8135610f8881610f55565b9392505050565b5f5b83811015610fa9578181015183820152602001610f91565b50505f910152565b5f8151808452610fc8816020860160208601610f8f565b601f01601f19169290920160200192915050565b602081525f610f886020830184610fb1565b5f60208284031215610ffe575f80fd5b5035919050565b80356001600160a01b038116811461101b575f80fd5b919050565b5f8060408385031215611031575f80fd5b61103a83611005565b946020939093013593505050565b5f805f6060848603121561105a575f80fd5b61106384611005565b925061107160208501611005565b9150604084013590509250925092565b5f60208284031215611091575f80fd5b610f8882611005565b828152604060208201525f6109316040830184610fb1565b634e487b7160e01b5f52604160045260245ffd5b5f67ffffffffffffffff808411156110e0576110e06110b2565b604051601f8501601f19908116603f01168101908282118183101715611108576111086110b2565b81604052809350858152868686011115611120575f80fd5b858560208301375f602087830101525050509392505050565b5f82601f830112611148575f80fd5b610f88838335602085016110c6565b5f8060408385031215611168575f80fd5b823567ffffffffffffffff8082111561117f575f80fd5b61118b86838701611139565b935060208501359150808211156111a0575f80fd5b506111ad85828601611139565b9150509250929050565b5f602082840312156111c7575f80fd5b813567ffffffffffffffff8111156111dd575f80fd5b61093184828501611139565b5f80604083850312156111fa575f80fd5b61120383611005565b915060208301358015158114611217575f80fd5b809150509250929050565b5f805f8060808587031215611235575f80fd5b61123e85611005565b935061124c60208601611005565b925060408501359150606085013567ffffffffffffffff81111561126e575f80fd5b8501601f8101871361127e575f80fd5b61128d878235602084016110c6565b91505092959194509250565b5f80604083850312156112aa575f80fd5b6112b383611005565b91506112c160208401611005565b90509250929050565b600181811c908216806112de57607f821691505b6020821081036112fc57634e487b7160e01b5f52602260045260245ffd5b50919050565b60208082526031908201527f4552433732313a207472616e736665722063616c6c6572206973206e6f74206f6040820152701ddb995c881b9bdc88185c1c1c9bdd9959607a1b606082015260800190565b6020808252600c908201526b4f6e6c7920666163746f727960a01b604082015260600190565b601f8211156104f8575f81815260208120601f850160051c8101602086101561139f5750805b601f850160051c820191505b818110156113be578281556001016113ab565b505050505050565b815167ffffffffffffffff8111156113e0576113e06110b2565b6113f4816113ee84546112ca565b84611379565b602080601f831160018114611427575f84156114105750858301515b5f19600386901b1c1916600185901b1785556113be565b5f85815260208120601f198616915b8281101561145557888601518255948401946001909101908401611436565b508582101561147257878501515f19600388901b60f8161c191681555b5050505050600190811b01905550565b5f80845461148f816112ca565b600182811680156114a757600181146114bc576114e8565b60ff19841687528215158302870194506114e8565b885f526020805f205f5b858110156114df5781548a8201529084019082016114c6565b50505082870194505b5050505083516114fc818360208801610f8f565b64173539b7b760d91b9101908152600501949350505050565b634e487b7160e01b5f52601160045260245ffd5b818103818111156102bc576102bc611515565b808201808211156102bc576102bc611515565b60208082526032908201527f4552433732313a207472616e7366657220746f206e6f6e20455243373231526560408201527131b2b4bb32b91034b6b83632b6b2b73a32b960711b606082015260800190565b5f600182016115b2576115b2611515565b5060010190565b634e487b7160e01b5f52601260045260245ffd5b5f826115db576115db6115b9565b500490565b5f826115ee576115ee6115b9565b500690565b634e487b7160e01b5f52603260045260245ffd5b6001600160a01b03858116825284166020820152604081018390526080606082018190525f9061163990830184610fb1565b9695505050505050565b5f60208284031215611653575f80fd5b8151610f8881610f5556fea26469706673582212202aca219efcfa6edc0c08f2e0dc20f58280156eb5716be104544eea685b61efba64736f6c63430008140033a2646970667358221220d6ba5703582def978857deeebab2c2da4f5bd2535ba9350f797ba515450712d364736f6c63430008140033",
}

// ContractABI is the input ABI used to generate the binding from.
// Deprecated: Use ContractMetaData.ABI instead.
var ContractABI = ContractMetaData.ABI

// ContractBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use ContractMetaData.Bin instead.
var ContractBin = ContractMetaData.Bin

// DeployContract deploys a new Ethereum contract, binding an instance of Contract to it.
func DeployContract(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Contract, error) {
	parsed, err := ContractMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(ContractBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Contract{ContractCaller: ContractCaller{contract: contract}, ContractTransactor: ContractTransactor{contract: contract}, ContractFilterer: ContractFilterer{contract: contract}}, nil
}

// Contract is an auto generated Go binding around an Ethereum contract.
type Contract struct {
	ContractCaller     // Read-only binding to the contract
	ContractTransactor // Write-only binding to the contract
	ContractFilterer   // Log filterer for contract events
}

// ContractCaller is an auto generated read-only Go binding around an Ethereum contract.
type ContractCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ContractTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ContractTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ContractFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ContractFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ContractSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ContractSession struct {
	Contract     *Contract         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ContractCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ContractCallerSession struct {
	Contract *ContractCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// ContractTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ContractTransactorSession struct {
	Contract     *ContractTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// ContractRaw is an auto generated low-level Go binding around an Ethereum contract.
type ContractRaw struct {
	Contract *Contract // Generic contract binding to access the raw methods on
}

// ContractCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ContractCallerRaw struct {
	Contract *ContractCaller // Generic read-only contract binding to access the raw methods on
}

// ContractTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ContractTransactorRaw struct {
	Contract *ContractTransactor // Generic write-only contract binding to access the raw methods on
}

// NewContract creates a new instance of Contract, bound to a specific deployed contract.
func NewContract(address common.Address, backend bind.ContractBackend) (*Contract, error) {
	contract, err := bindContract(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Contract{ContractCaller: ContractCaller{contract: contract}, ContractTransactor: ContractTransactor{contract: contract}, ContractFilterer: ContractFilterer{contract: contract}}, nil
}

// NewContractCaller creates a new read-only instance of Contract, bound to a specific deployed contract.
func NewContractCaller(address common.Address, caller bind.ContractCaller) (*ContractCaller, error) {
	contract, err := bindContract(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ContractCaller{contract: contract}, nil
}

// NewContractTransactor creates a new write-only instance of Contract, bound to a specific deployed contract.
func NewContractTransactor(address common.Address, transactor bind.ContractTransactor) (*ContractTransactor, error) {
	contract, err := bindContract(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ContractTransactor{contract: contract}, nil
}

// NewContractFilterer creates a new log filterer instance of Contract, bound to a specific deployed contract.
func NewContractFilterer(address common.Address, filterer bind.ContractFilterer) (*ContractFilterer, error) {
	contract, err := bindContract(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ContractFilterer{contract: contract}, nil
}

// bindContract binds a generic wrapper to an already deployed contract.
func bindContract(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := ContractMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Contract *ContractRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Contract.Contract.ContractCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Contract *ContractRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Contract.Contract.ContractTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Contract *ContractRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Contract.Contract.ContractTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Contract *ContractCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Contract.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Contract *ContractTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Contract.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Contract *ContractTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Contract.Contract.contract.Transact(opts, method, params...)
}

// Collections is a free data retrieval call binding the contract method 0x66e54d7b.
//
// Solidity: function collections(string ) view returns(address)
func (_Contract *ContractCaller) Collections(opts *bind.CallOpts, arg0 string) (common.Address, error) {
	var out []interface{}
	err := _Contract.contract.Call(opts, &out, "collections", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Collections is a free data retrieval call binding the contract method 0x66e54d7b.
//
// Solidity: function collections(string ) view returns(address)
func (_Contract *ContractSession) Collections(arg0 string) (common.Address, error) {
	return _Contract.Contract.Collections(&_Contract.CallOpts, arg0)
}

// Collections is a free data retrieval call binding the contract method 0x66e54d7b.
//
// Solidity: function collections(string ) view returns(address)
func (_Contract *ContractCallerSession) Collections(arg0 string) (common.Address, error) {
	return _Contract.Contract.Collections(&_Contract.CallOpts, arg0)
}

// CreateCollection is a paid mutator transaction binding the contract method 0x176b9b75.
//
// Solidity: function createCollection(string _name, string _symbol) returns(address collection)
func (_Contract *ContractTransactor) CreateCollection(opts *bind.TransactOpts, _name string, _symbol string) (*types.Transaction, error) {
	return _Contract.contract.Transact(opts, "createCollection", _name, _symbol)
}

// CreateCollection is a paid mutator transaction binding the contract method 0x176b9b75.
//
// Solidity: function createCollection(string _name, string _symbol) returns(address collection)
func (_Contract *ContractSession) CreateCollection(_name string, _symbol string) (*types.Transaction, error) {
	return _Contract.Contract.CreateCollection(&_Contract.TransactOpts, _name, _symbol)
}

// CreateCollection is a paid mutator transaction binding the contract method 0x176b9b75.
//
// Solidity: function createCollection(string _name, string _symbol) returns(address collection)
func (_Contract *ContractTransactorSession) CreateCollection(_name string, _symbol string) (*types.Transaction, error) {
	return _Contract.Contract.CreateCollection(&_Contract.TransactOpts, _name, _symbol)
}

// Mint is a paid mutator transaction binding the contract method 0x6a627842.
//
// Solidity: function mint(address _collection) returns()
func (_Contract *ContractTransactor) Mint(opts *bind.TransactOpts, _collection common.Address) (*types.Transaction, error) {
	return _Contract.contract.Transact(opts, "mint", _collection)
}

// Mint is a paid mutator transaction binding the contract method 0x6a627842.
//
// Solidity: function mint(address _collection) returns()
func (_Contract *ContractSession) Mint(_collection common.Address) (*types.Transaction, error) {
	return _Contract.Contract.Mint(&_Contract.TransactOpts, _collection)
}

// Mint is a paid mutator transaction binding the contract method 0x6a627842.
//
// Solidity: function mint(address _collection) returns()
func (_Contract *ContractTransactorSession) Mint(_collection common.Address) (*types.Transaction, error) {
	return _Contract.Contract.Mint(&_Contract.TransactOpts, _collection)
}

// ContractCollectionCreatedIterator is returned from FilterCollectionCreated and is used to iterate over the raw logs and unpacked data for CollectionCreated events raised by the Contract contract.
type ContractCollectionCreatedIterator struct {
	Event *ContractCollectionCreated // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ContractCollectionCreatedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ContractCollectionCreated)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ContractCollectionCreated)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ContractCollectionCreatedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ContractCollectionCreatedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ContractCollectionCreated represents a CollectionCreated event raised by the Contract contract.
type ContractCollectionCreated struct {
	Collection common.Address
	Name       string
	Symbol     string
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterCollectionCreated is a free log retrieval operation binding the contract event 0x3454b57f2dca4f5a54e8358d096ac9d1a0d2dab98991ddb89ff9ea1746260617.
//
// Solidity: event CollectionCreated(address indexed collection, string name, string symbol)
func (_Contract *ContractFilterer) FilterCollectionCreated(opts *bind.FilterOpts, collection []common.Address) (*ContractCollectionCreatedIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}

	logs, sub, err := _Contract.contract.FilterLogs(opts, "CollectionCreated", collectionRule)
	if err != nil {
		return nil, err
	}
	return &ContractCollectionCreatedIterator{contract: _Contract.contract, event: "CollectionCreated", logs: logs, sub: sub}, nil
}

// WatchCollectionCreated is a free log subscription operation binding the contract event 0x3454b57f2dca4f5a54e8358d096ac9d1a0d2dab98991ddb89ff9ea1746260617.
//
// Solidity: event CollectionCreated(address indexed collection, string name, string symbol)
func (_Contract *ContractFilterer) WatchCollectionCreated(opts *bind.WatchOpts, sink chan<- *ContractCollectionCreated, collection []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}

	logs, sub, err := _Contract.contract.WatchLogs(opts, "CollectionCreated", collectionRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ContractCollectionCreated)
				if err := _Contract.contract.UnpackLog(event, "CollectionCreated", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseCollectionCreated is a log parse operation binding the contract event 0x3454b57f2dca4f5a54e8358d096ac9d1a0d2dab98991ddb89ff9ea1746260617.
//
// Solidity: event CollectionCreated(address indexed collection, string name, string symbol)
func (_Contract *ContractFilterer) ParseCollectionCreated(log types.Log) (*ContractCollectionCreated, error) {
	event := new(ContractCollectionCreated)
	if err := _Contract.contract.UnpackLog(event, "CollectionCreated", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ContractTokenMintedIterator is returned from FilterTokenMinted and is used to iterate over the raw logs and unpacked data for TokenMinted events raised by the Contract contract.
type ContractTokenMintedIterator struct {
	Event *ContractTokenMinted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ContractTokenMintedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ContractTokenMinted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ContractTokenMinted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ContractTokenMintedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ContractTokenMintedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ContractTokenMinted represents a TokenMinted event raised by the Contract contract.
type ContractTokenMinted struct {
	Collection common.Address
	Recipient  common.Address
	TokenId    *big.Int
	TokenUri   string
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterTokenMinted is a free log retrieval operation binding the contract event 0xc9fee7cd4889f66f10ff8117316524260a5242e88e25e0656dfb3f4196a21917.
//
// Solidity: event TokenMinted(address indexed collection, address recipient, uint256 tokenId, string tokenUri)
func (_Contract *ContractFilterer) FilterTokenMinted(opts *bind.FilterOpts, collection []common.Address) (*ContractTokenMintedIterator, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}

	logs, sub, err := _Contract.contract.FilterLogs(opts, "TokenMinted", collectionRule)
	if err != nil {
		return nil, err
	}
	return &ContractTokenMintedIterator{contract: _Contract.contract, event: "TokenMinted", logs: logs, sub: sub}, nil
}

// WatchTokenMinted is a free log subscription operation binding the contract event 0xc9fee7cd4889f66f10ff8117316524260a5242e88e25e0656dfb3f4196a21917.
//
// Solidity: event TokenMinted(address indexed collection, address recipient, uint256 tokenId, string tokenUri)
func (_Contract *ContractFilterer) WatchTokenMinted(opts *bind.WatchOpts, sink chan<- *ContractTokenMinted, collection []common.Address) (event.Subscription, error) {

	var collectionRule []interface{}
	for _, collectionItem := range collection {
		collectionRule = append(collectionRule, collectionItem)
	}

	logs, sub, err := _Contract.contract.WatchLogs(opts, "TokenMinted", collectionRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ContractTokenMinted)
				if err := _Contract.contract.UnpackLog(event, "TokenMinted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTokenMinted is a log parse operation binding the contract event 0xc9fee7cd4889f66f10ff8117316524260a5242e88e25e0656dfb3f4196a21917.
//
// Solidity: event TokenMinted(address indexed collection, address recipient, uint256 tokenId, string tokenUri)
func (_Contract *ContractFilterer) ParseTokenMinted(log types.Log) (*ContractTokenMinted, error) {
	event := new(ContractTokenMinted)
	if err := _Contract.contract.UnpackLog(event, "TokenMinted", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
