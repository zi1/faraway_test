// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

interface INFTFactory {
    /**
     * @notice Create collection
     * @param _name: collection name
     * @param _symbol: collection symbol
     *
     */
    function createCollection(
        string memory _name,
        string memory _symbol
    ) external returns (address);

    /**
     * @notice Mint collection
     * @param _collection: collection address
     *
     */
    function mint(
        address _collection
    ) external;
}
