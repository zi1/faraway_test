// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

interface INFTCollection {
    /**
     * @notice Initialize nft collection.
     * @param _name: collection name
     * @param _symbol: collection symbol
     *
     * @dev Callable by factory
     *
     */
    function initialize(
        string memory _name,
        string memory _symbol
    ) external;

    /**
     * @notice Mint nft.
     * @param _to: recipient address
     *
     * @dev Callable by factory
     *
     */
    function safeMint(
        address _to
    ) external returns (
        uint256,
        string memory
    );

    /**
     * @notice Setting base uri for nft collection.
     * @param _uri: new base uri
     *
     * @dev Callable by factory
     *
     */
    function setUri(
        string memory _uri
    ) external;

    /**
     * @notice Return token URI
     * @param tokenId: token id
     *
     */
    function tokenURI(
        uint256 tokenId
    ) external view returns (string memory);
}
