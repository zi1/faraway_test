// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import '../vendor/lib/INFTCollection.sol';
import "../vendor/openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../vendor/openzeppelin/contracts/utils/Counters.sol";
import "../vendor/openzeppelin/contracts/utils/Strings.sol";

contract NFTCollection is INFTCollection, ERC721 {
    using Counters for Counters.Counter;

    string private NAME;
    string private SYMBOL;
    string private baseURI;
    Counters.Counter private _tokenIdCounter;
    address factory;

    constructor() ERC721('NFTFactoryCollection', 'NFTFC') {
        factory = msg.sender;
    }

    /**
     * @notice Initialize nft collection.
     * @param _name: collection name
     * @param _symbol: collection symbol
     *
     * @dev Callable by factory
     *
     */
    function initialize(
        string memory _name,
        string memory _symbol
    ) external {
        require(msg.sender == factory, 'Only factory');
        NAME = _name;
        SYMBOL = _symbol;
    }

    /**
     * @notice Mint nft.
     * @param _to: recipient address
     *
     * @dev Callable by factory
     *
     */
    function safeMint(
        address _to
    ) external returns (
        uint256 tokenId,
        string memory tokenUri
    ) {
        require(msg.sender == factory, 'Only factory');

        tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(_to, tokenId);
        tokenUri = tokenURI(tokenId);
    }

    /**
     * @notice Setting base uri for nft collection.
     * @param _uri: new base uri
     *
     * @dev Callable by factory
     *
     */
    function setUri(
        string memory _uri
    ) external {
        require(msg.sender == factory, 'Only factory');
        baseURI = _uri;
    }

    /**
     * @notice Return base URI
     *
     */
    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }

    /**
     * @notice Return token URI
     * @param tokenId: token id
     *
     */
    function tokenURI(
        uint256 tokenId
    ) public view virtual override(ERC721, INFTCollection) returns (string memory) {
        return bytes(baseURI).length > 0 ? string(abi.encodePacked(baseURI, Strings.toString(tokenId), '.json')) : '';
    }
}
