// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import '../vendor/lib/INFTFactory.sol';
import './NFTCollection.sol';

contract NFTFactory is INFTFactory {

    // user
    // is_blacklisted
    mapping(string => address) public collections;

    event CollectionCreated(address indexed collection, string name, string symbol);
    event TokenMinted(address indexed collection, address recipient, uint256 tokenId, string tokenUri);

    /**
     * @notice Create collection
     * @param _name: collection name
     * @param _symbol: collection symbol
     *
     */
    function createCollection(
        string memory _name,
        string memory _symbol
    ) external returns (address collection) {
        require(collections[_symbol] == address(0), 'collection is required');
        bytes memory bytecode = type(NFTCollection).creationCode;
        bytes32 salt = keccak256(abi.encodePacked(_name, _symbol));
        assembly {
            collection := create2(0, add(bytecode, 32), mload(bytecode), salt)
        }

        INFTCollection(collection).initialize(_name, _symbol);
        collections[_symbol] = collection;
        emit CollectionCreated(collection, _name, _symbol);
    }

    /**
     * @notice Mint collection
     * @param _collection: collection address
     *
     */
    function mint(
        address _collection
    ) external {
        require(_collection != address(0), 'collection is required');
        (uint256 tokenId, string memory tokenUri) = INFTCollection(_collection).safeMint(msg.sender);

        emit TokenMinted(_collection, msg.sender, tokenId, tokenUri);
    }
}
